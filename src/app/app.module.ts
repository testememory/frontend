import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CrIlustradoresComponent } from './cr-ilustradores/cr-ilustradores.component';
import { CrAutoresComponent } from './cr-autores/cr-autores.component';
import { CrHqsComponent } from './cr-hqs/cr-hqs.component';
import { ListaIlustradoresComponent } from './lista-ilustradores/lista-ilustradores.component';
import { ListaAutoresComponent } from './lista-autores/lista-autores.component';
import { ListaHqsComponent } from './lista-hqs/lista-hqs.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';

@NgModule({
  declarations: [
    AppComponent,
    CrIlustradoresComponent,
    CrAutoresComponent,
    CrHqsComponent,
    ListaIlustradoresComponent,
    ListaAutoresComponent,
    ListaHqsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatToolbarModule,
    MatIconModule,
    MatTabsModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSelectModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
