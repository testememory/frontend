import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrAutoresComponent } from './cr-autores.component';

describe('CrAutoresComponent', () => {
  let component: CrAutoresComponent;
  let fixture: ComponentFixture<CrAutoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrAutoresComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CrAutoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
