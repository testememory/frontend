import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AutoresService } from '../services/autores.service';

@Component({
  selector: 'app-cr-autores',
  templateUrl: './cr-autores.component.html',
  styleUrls: ['./cr-autores.component.scss']
})
export class CrAutoresComponent implements OnInit {

  form!: FormGroup;
  
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private dialogRef: MatDialogRef<CrAutoresComponent>,
    private fb: FormBuilder, 
        public autoresService: AutoresService
  ) { }

  ngOnInit(): void {
    this.createForm();
  }

  // Criar o formulário responsivo
  createForm(): void {
    this.form = this.fb.group({
      nome: [this.data?.nome],
      idade: [this.data?.idade],
      obras: [this.data?.obras],
    })
  }

  // Deleta registro
  deleteAutor(): void {
    this.autoresService.deleteAutor(this.data.id).subscribe(res => {
      console.log(res);
      this.dialogRef.close();
    })
  }

  submit(): void {
    // Atualiza registro
    if(this.data?.id) {
      this.autoresService.updateAutor(this.data.id, this.form.value).subscribe(res => {
        console.log(res);
        this.dialogRef.close();
      })
    }
    // Salva novo registro
    else {
      this.autoresService.saveAutor(this.form.value).subscribe(res => {
        console.log(res);
        this.dialogRef.close();
      })
    }
  }
}
