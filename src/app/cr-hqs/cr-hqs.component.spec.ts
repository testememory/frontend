import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrHqsComponent } from './cr-hqs.component';

describe('CrHqsComponent', () => {
  let component: CrHqsComponent;
  let fixture: ComponentFixture<CrHqsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrHqsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CrHqsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
