import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AutoresService } from '../services/autores.service';
import { HqsService } from '../services/hqs.service';
import { IlustradoresService } from '../services/ilustradores.service';

@Component({
  selector: 'app-cr-hqs',
  templateUrl: './cr-hqs.component.html',
  styleUrls: ['./cr-hqs.component.scss']
})
export class CrHqsComponent implements OnInit {

  form!: FormGroup;
  autores!: any[];
  ilustradores!: any[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private dialogRef: MatDialogRef<CrHqsComponent>,
    private fb: FormBuilder, 
    public hqsService: HqsService,
    public ilustradoresService: IlustradoresService,
    public autoresService: AutoresService
  ) { }

  ngOnInit(): void {
    this.createForm();
    this.getAutores();
    this.getIlustradores();
  }

  // Criar o formulário responsivo
  createForm(): void {
    this.form = this.fb.group({
      titulo: [this.data?.titulo],
      preco: [this.data?.preco],
      genero: [this.data?.genero],
      id_autor: [this.data?.id_autor],
      id_ilustrador: [this.data?.id_ilustrador]
    })
  }

  // Preenche array de autores
  getAutores(): void {
    this.autoresService.listAutores().subscribe(res => {
      this.autores = res;
    })
  }

  // Preenche array de ilustradores
  getIlustradores(): void {
    this.ilustradoresService.listIlustradores().subscribe(res => {
      this.ilustradores = res;
    })
  }

  // Deleta registro
  deleteHq(): void {
    this.hqsService.deleteHq(this.data.id).subscribe(res => {
      console.log(res);
      this.dialogRef.close();
    })
  }

  submit(): void {
    // Atualiza registro
    if(this.data?.id) {
      this.hqsService.updateHq(this.data.id, this.form.value).subscribe(res => {
        console.log(res);
        this.dialogRef.close();
      })
    }
    // Salva novo registro
    else {
      this.hqsService.saveHq(this.form.value).subscribe(res => {
        console.log(res);
        this.dialogRef.close();
      })
    }
  }

}
