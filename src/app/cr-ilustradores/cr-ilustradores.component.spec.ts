import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrIlustradoresComponent } from './cr-ilustradores.component';

describe('CrIlustradoresComponent', () => {
  let component: CrIlustradoresComponent;
  let fixture: ComponentFixture<CrIlustradoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrIlustradoresComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CrIlustradoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
