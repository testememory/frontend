import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IlustradoresService } from '../services/ilustradores.service';

@Component({
  selector: 'app-cr-ilustradores',
  templateUrl: './cr-ilustradores.component.html',
  styleUrls: ['./cr-ilustradores.component.scss']
})
export class CrIlustradoresComponent implements OnInit {

 
  form!: FormGroup;
  
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private dialogRef: MatDialogRef<CrIlustradoresComponent>,
    private fb: FormBuilder, 
        public ilustrdoresService: IlustradoresService
  ) { }

  ngOnInit(): void {
    this.createForm();
  }

  // Criar o formulário responsivo
  createForm(): void {
    this.form = this.fb.group({
      nome: [this.data?.nome],
      idade: [this.data?.idade],
      obras: [this.data?.obras],
    })
  }

  // Deleta registro
  deleteIlustrador(): void {
    this.ilustrdoresService.deleteIlustrador(this.data.id).subscribe(res => {
      console.log(res);
      this.dialogRef.close();
    })
  }

  submit(): void {
    // Atualiza registro
    if(this.data?.id) {
      this.ilustrdoresService.updateIlustrador(this.data.id, this.form.value).subscribe(res => {
        console.log(res);
        this.dialogRef.close();
      })
    }
    // Salva novo registro
    else {
      this.ilustrdoresService.saveIlustrador(this.form.value).subscribe(res => {
        console.log(res);
        this.dialogRef.close();
      })
    }
  }
}
