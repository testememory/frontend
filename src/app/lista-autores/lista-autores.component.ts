import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CrAutoresComponent } from '../cr-autores/cr-autores.component';
import { AutoresService } from '../services/autores.service';

@Component({
  selector: 'app-lista-autores',
  templateUrl: './lista-autores.component.html',
  styleUrls: ['./lista-autores.component.scss']
})
export class ListaAutoresComponent implements OnInit {
  displayedColumns: string[] = ['nome', 'idade', 'obras'];
  dataSource = [];

  constructor(private autoresService: AutoresService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.insertData();
  }

  // Preenche tabela
  insertData(): void {
    this.autoresService.listAutores().subscribe(res => {
      this.dataSource = res;
    });
  }

  // Botão de criar novo registro
  create(): void {
    this.dialog.open(CrAutoresComponent)
    .afterClosed().subscribe(() => {
      this.insertData();
    });
  }

  // Seleciona registro
  selected(value: any) {
    let autor = {};
    this.autoresService.listAutorById(value.id).subscribe(res => {
      autor = res.response[0];
      this.dialog.open(CrAutoresComponent, {
        data: autor
      })
      .afterClosed().subscribe(() => {
        this.insertData();
      });
    });
  }
}