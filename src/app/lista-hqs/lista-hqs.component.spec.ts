import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaHqsComponent } from './lista-hqs.component';

describe('ListaHqsComponent', () => {
  let component: ListaHqsComponent;
  let fixture: ComponentFixture<ListaHqsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaHqsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListaHqsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
