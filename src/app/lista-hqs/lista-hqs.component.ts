import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CrHqsComponent } from '../cr-hqs/cr-hqs.component';
import { HqsService } from '../services/hqs.service';

@Component({
  selector: 'app-lista-hqs',
  templateUrl: './lista-hqs.component.html',
  styleUrls: ['./lista-hqs.component.scss']
})
export class ListaHqsComponent implements OnInit {
  displayedColumns: string[] = ['titulo', 'preco', 'genero'];
  dataSource = [];

  constructor(private hqsService: HqsService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.insertData();
  }

  // Preenche tabela
  insertData(): void {
    this.hqsService.listHqs().subscribe(res => {
      this.dataSource = res;
    });
  }

  // Botão de criar novo registro
  create(): void {
    this.dialog.open(CrHqsComponent)
    .afterClosed().subscribe(() => {
      this.insertData();
    });
  }

  // Seleciona registro
  selected(value: any) {
    let hq = {};
    this.hqsService.listHqById(value.id).subscribe(res => {
      hq = res.response[0];
      this.dialog.open(CrHqsComponent, {
        data: hq
      })
      .afterClosed().subscribe(() => {
        this.insertData();
      });
    });
  }
 
}
