import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaIlustradoresComponent } from './lista-ilustradores.component';

describe('ListaIlustradoresComponent', () => {
  let component: ListaIlustradoresComponent;
  let fixture: ComponentFixture<ListaIlustradoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaIlustradoresComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListaIlustradoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
