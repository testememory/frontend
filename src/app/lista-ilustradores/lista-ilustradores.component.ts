import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CrIlustradoresComponent } from '../cr-ilustradores/cr-ilustradores.component';
import { IlustradoresService } from '../services/ilustradores.service';

@Component({
  selector: 'app-lista-ilustradores',
  templateUrl: './lista-ilustradores.component.html',
  styleUrls: ['./lista-ilustradores.component.scss']
})
export class ListaIlustradoresComponent implements OnInit {
  displayedColumns: string[] = ['nome', 'idade', 'obras'];
  dataSource = [];

  constructor(private ilustradoresService: IlustradoresService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.insertData();
  }

  // Preenche tabela
  insertData(): void {
    this.ilustradoresService.listIlustradores().subscribe(res => {
      this.dataSource = res;
    });
  }

  // Botão de criar novo registro
  create(): void {
    this.dialog.open(CrIlustradoresComponent)
    .afterClosed().subscribe(() => {
      this.insertData();
    });
  }

  // Seleciona registro
  selected(value: any) {
    let hq = {};
    this.ilustradoresService.listIlustradorById(value.id).subscribe(res => {
      hq = res.response[0];
      this.dialog.open(CrIlustradoresComponent, {
        data: hq
      })
      .afterClosed().subscribe(() => {
        this.insertData();
      });
    });
  }
}