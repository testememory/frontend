import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AutoresService {

  private readonly api = "http://localhost:3000/api/autores/";

  constructor(private http: HttpClient) { }

  listAutores(): Observable<any> {
    return this.http.get(this.api);
  }

  listAutorById(id: number): Observable<any> {
    return this.http.get(this.api + id);
  }

  saveAutor(hq: any): Observable<any> {
    return this.http.post(this.api, hq);
  }

  updateAutor(id: number, hq: any): Observable<any> {
    return this.http.patch(this.api + id, hq);
  }

  deleteAutor(id: number): Observable<any> {
    return this.http.delete(this.api + id);
  }
}


