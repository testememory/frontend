import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HqsService {

  private readonly api = "http://localhost:3000/api/hqs/";

  constructor(private http: HttpClient) { }

  listHqs(): Observable<any> {
    return this.http.get(this.api);
  }

  listHqById(id: number): Observable<any> {
    return this.http.get(this.api + id);
  }

  saveHq(hq: any): Observable<any> {
    return this.http.post(this.api, hq);
  }

  updateHq(id: number, hq: any): Observable<any> {
    return this.http.patch(this.api + id, hq);
  }

  deleteHq(id: number): Observable<any> {
    return this.http.delete(this.api + id);
  }
}
