import { TestBed } from '@angular/core/testing';

import { IlustradoresService } from './ilustradores.service';

describe('IlustradoresService', () => {
  let service: IlustradoresService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IlustradoresService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
