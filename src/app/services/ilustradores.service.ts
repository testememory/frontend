import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IlustradoresService {

  private readonly api = "http://localhost:3000/api/ilustradores/";

  constructor(private http: HttpClient) { }

  listIlustradores(): Observable<any> {
    return this.http.get(this.api);
  } 
  
  listIlustradorById(id: number): Observable<any> {
    return this.http.get(this.api + id);
  }

  saveIlustrador(hq: any): Observable<any> {
    return this.http.post(this.api, hq);
  }

  updateIlustrador(id: number, hq: any): Observable<any> {
    return this.http.patch(this.api + id, hq);
  }

  deleteIlustrador(id: number): Observable<any> {
    return this.http.delete(this.api + id);
  }
}
